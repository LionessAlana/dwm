/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 5;       /* snap pixel */
static const unsigned int minwsz    = 15;       /* Minimal heigt of a client for smfact */
static const int toffset		   	= 20;   	/* tag text left offset, 0 is default*/
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Drift:size=10" };
static const char dmenufont[]       = "Hack:size=8";
static const unsigned int gappx		= 10; 		/* pixel gap between windows */
static const char col_gray1[]       = "#151515";
static const char col_gray2[]       = "#c45d2f";
static const char col_gray3[]       = "#d1924c";
static const char col_gray4[]       = "#c04539";
static const char col_cyan[]	    = "#d5a45c";
static const char *colors[][3]      = {
	/*               fg         bg         	border   */
	[SchemeNorm] = { col_gray2,	col_gray1, 	col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray1, 	col_gray3  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      		instance    title       tags mask     isfloating   	monitor */
	{ "Gimp",     		NULL,       NULL,       0,            1,           	-1 },
	{ "Firefox",  		NULL,       NULL,       1 << 8,       0,           	-1 },
	{ "slickpicker", 	NULL, 	  	NULL,		0,			  1,		   	-1 },
	{ "Franz",	  		NULL,		NULL,		1 << 1, 	  0,			-1 },
	{ "qutebrowser",  	NULL,   	NULL,		1 << 2, 	  0,			-1 },
	{ "spotify",	  	NULL,   	"Spotify",	1 << 3, 	  0,			-1 },
	{ "Tor Browser",  	NULL,   	NULL,		1 << 8, 	  0,			-1 },
	{ NULL,			  	NULL,   	"qBittorent",1 << 8, 	  0,			-1 },
};

/* layout(s) */
static const float mfact     = 0.61; /* factor of master area size [0.05..0.95] */
static const float smfact    = 0.2; /* factor of tiled clients [0.00..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[=]",      tile },    /* first entry is default */
	{ "[X]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[T]",      bstack },
	{ "[-]",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };

static const char *roficmd[] = { "rofi", "-show", "combi", "-show-icons", NULL };

static const char *pwrcmd[] = { "rofi", "-show", "power", NULL };

static const char *volinc[] = { "pamixer", "-u", "-i", "5", NULL };
static const char *voldec[] = { "pamixer", "-u", "-d", "5", NULL };
static const char *voltog[] = { "pamixer", "-t", NULL };

static const char *xblinc[] = { "xbacklight", "-inc", "5", NULL };
static const char *xbldec[] = { "xbacklight", "-", "5", NULL };

static const char *scrcmd[] = { "/home/alana/Documents/scripts/scrot.sh", NULL };

static const char *termcmd[]  = { "xst", NULL };

static Key keys[] = {
	/* modifier                 key        					function        	argument */
	{ Mod1Mask, 		        XK_F2, 	   	   				spawn,      	 	{.v = roficmd} },
	{ Mod2Mask, 		        XF86XK_PowerOff, 			spawn,   	 		{.v = pwrcmd} },
	{ Mod2Mask, 		        XF86XK_AudioLowerVolume, 	spawn,    			{.v = voldec} },
	{ Mod2Mask, 		        XF86XK_AudioRaiseVolume, 	spawn,    			{.v = volinc} },
	{ Mod2Mask, 		        XF86XK_AudioMute, 			spawn,    			{.v = voltog} },
	{ Mod2Mask, 		        XF86XK_MonBrightnessDown,	spawn,    			{.v = xbldec} },
	{ Mod2Mask, 		        XF86XK_MonBrightnessUp, 	spawn,    			{.v = xblinc} },
	{ MODKEY,                   XK_p,      					spawn,	        	{.v = roficmd } },
	{ MODKEY, 		            XK_Return, 					spawn,          	{.v = termcmd } },
	{ Mod2Mask,		            XK_Print, 					spawn,          	{.v = scrcmd } },
	{ MODKEY|ShiftMask,         XK_Return, 					zoom,           	{0} },
	{ Mod1Mask,		            XK_Tab,	   					view,           	{0} },
	{ MODKEY,                   XK_b,      					togglebar,      	{0} },
	{ MODKEY,                   XK_k,      					focusstack,     	{.i = +1 } },
	{ MODKEY,                   XK_j,      					focusstack,     	{.i = -1 } },
	{ MODKEY|ControlMask,       XK_k,      					pushdown,     		{0} },
	{ MODKEY|ControlMask,       XK_j,      					pushup,     		{0} },
	{ MODKEY,                   XK_Right,  					focusstack,     	{.i = +1 } },
	{ MODKEY,                   XK_Left,   					focusstack,     	{.i = -1 } },
	{ MODKEY,                   XK_i,      					incnmaster,     	{.i = +1 } },
	{ MODKEY,                   XK_d,      					incnmaster,     	{.i = -1 } },
	{ MODKEY,                   XK_h,      					setmfact,       	{.f = -0.05} },
	{ MODKEY,                   XK_l,      					setmfact,       	{.f = +0.05} },
	{ MODKEY|ShiftMask,         XK_h,      					setsmfact,      	{.f = +0.05} },
	{ MODKEY|ShiftMask,         XK_l,      					setsmfact,      	{.f = -0.05} },
	{ MODKEY,                   XK_Tab,    					zoom,           	{0} },
	{ MODKEY|ShiftMask,         XK_c,      					killclient,     	{0} },
	{ MODKEY, 			        XK_q,      					killclient,     	{0} },
	{ MODKEY,                   XK_t,      					setlayout,      	{.v = &layouts[0]} },
	{ MODKEY,                   XK_f,      					setlayout,      	{.v = &layouts[1]} },
	{ MODKEY,                   XK_m,      					setlayout,      	{.v = &layouts[2]} },
	{ MODKEY,                   XK_u,        				setlayout,      	{.v = &layouts[3]} },
	{ MODKEY,                   XK_space,  					togglefloating, 	{0} },
	{ MODKEY|ShiftMask,         XK_space,  					setlayout,	   		{0} },
	{ MODKEY,                   XK_0,      					view,           	{.ui = ~0 } },
	{ MODKEY|ShiftMask,         XK_0,      					tag,            	{.ui = ~0 } },
	{ MODKEY,                   XK_comma,  					focusmon,       	{.i = -1 } },
	{ MODKEY,                   XK_period, 					focusmon,       	{.i = +1 } },
	{ MODKEY|ShiftMask,         XK_comma,  					tagmon,         	{.i = -1 } },
	{ MODKEY|ShiftMask,         XK_period, 					tagmon,         	{.i = +1 } },
	{ MODKEY|ShiftMask,         XK_q,      					quit,           	{0} },
	TAGKEYS(                    XK_1,                      						0)
	TAGKEYS(                    XK_2,                      						1)
	TAGKEYS(                    XK_3,                      						2)
	TAGKEYS(                    XK_4,                      						3)
	TAGKEYS(                    XK_5,                      						4)
	TAGKEYS(                    XK_6,                      						5)
	TAGKEYS(                    XK_7,                      						6)
	TAGKEYS(                    XK_8,                      						7)
	TAGKEYS(                    XK_9,                      						8)
	TAGKEYS(                    XK_KP_End,   	           						0)
	TAGKEYS(                    XK_KP_Down,    	           						1)
	TAGKEYS(                    XK_KP_Next,        	       						2)
	TAGKEYS(                    XK_KP_Left,            	   						3)
	TAGKEYS(                    XK_KP_Begin,       	       						4)
	TAGKEYS(                    XK_KP_Right,               						5)
	TAGKEYS(                    XK_KP_Home,                						6)
	TAGKEYS(                    XK_KP_Up,                  						7)
	TAGKEYS(                    XK_KP_Prior,               						8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

