#!/bin/bash
spacer="   "

while true;
do
	STR=""
	
	# Volume
	mute="$( pamixer --get-mute )"

	if [ "$mute" == "false" ]; then
		vol="$( pamixer --get-volume )"
		STR+="|  vol $vol%"
	else
		STR+="|  muted"
	fi
	STR+="$spacer"

	# Backlight
#	STR+="$( xbacklight | awk -F "." '{print "| scr " $1 "%" }' )"
#	STR+="$spacer"

	# Temp
	sum=0
	sens="$( sensors | awk -F '+|°|\n' '/Core/ { sum += $2 } END { printf "%3.0d", sum / 4 }' )"

	STR+="| "$sens"°C"$spacer

	# Connection
	conn="$( nmcli device status | awk -v s="connected" '$0 ~ s { print "online" }' )"
	if [[ $conn != "" ]]; then
		STR+="|  "$conn$spacer
	else
		STR+="|  "offline$spacer
	fi

	# Date/Time
	STR+=$( date +'|  %a, %d %b, %Y   |  %R' )
	STR+="  "
	
	xsetroot -name "$STR"
	sleep 1
done
